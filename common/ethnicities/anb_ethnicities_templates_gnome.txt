﻿# Receives data from /genes
# Ethnicity templates are used in other ethnicities and templates in this folder

gnome = {

	@neg1_min = 0.4
	@neg1_max = 0.5

	@neg2_min = 0.3
	@neg2_max = 0.4

	@neg3_min = 0.1
	@neg3_max = 0.3

	@pos1_min = 0.5
	@pos1_max = 0.6

	@pos2_min = 0.6
	@pos2_max = 0.7

	@pos3_min = 0.7
	@pos3_max = 0.9

	@beauty1min = 0.35
	@beauty1max = 0.65

	@beauty2min = 0.4
	@beauty2max = 0.6

	@beauty3min = 0.45
	@beauty3max = 0.55

	@blend1min = 0.0
	@blend1max = 0.2

	@blend2min = 0.2
	@blend2max = 0.5

	@blend3min = 0.5
	@blend3max = 0.8

	skin_color = {
		10 = { 0.0 0.05 0.2 0.1 }
	}
	eye_color = {
        # Brown
        5 = { 0.05 0.5 0.33 0.8 }
        # Green
        35 = { 0.33 0.5 0.67 0.8 }
        # Blue
        20 = { 0.67 0.5 1.0 0.8 }
	}
	hair_color = {
		# Blonde
		20 = { 0.4 0.25 0.75 0.5 }
		# Brown
		10 = { 0.65 0.5 0.9 0.8 }
		# Red
		15 = { 0.85 0.0 0.99 0.5 }
		# Black
		5 = { 0.0 0.9 0.5 1.0 }
		# Rainbow
		15 = { 1.0 0.0 1.0 1.0 }
	}

	### Additive anim genes ###
    gene_chin_forward = {


        47 = { name = chin_forward   range = { 0.2 0.45 }      }
        84 = { name = chin_forward   range = { 0.45 0.55 }      }   #weaker chins in general
        7 = { name = chin_forward   range = { 0.55 0.7 }      }


    }

    gene_chin_height = {

        84 = { name = chin_height   range = { 0.45 0.55 }      }
        7 = { name = chin_height   range = { 0.55 0.7 }      }  #has chin


    }

    gene_chin_width = {


        17 = { name = chin_width   range = { 0.3 0.45 }      }  #thinner chin
        64 = { name = chin_width   range = { 0.4 0.5 }      }


    }

    gene_eye_angle = {

        7 = { name = eye_angle   range = { 0.3 0.45 }      }
        84 = { name = eye_angle   range = { 0.45 0.55 }      }
        7 = { name = eye_angle   range = { 0.55 0.7 }      }

    }

    gene_eye_depth = {

        7 = { name = eye_depth   range = { 0.2 0.3 }      }
        27 = { name = eye_depth   range = { 0.3 0.4 }      }
        54 = { name = eye_depth   range = { 0.4 0.5 }      }  #eyes are more forward


    }

    gene_eye_distance = {

        74 = { name = eye_distance   range = { 0.5 0.6 }      }
        12 = { name = eye_distance   range = { 0.6 0.7 }      }    #farther out in general


    }

    gene_eye_height = {


        64 = { name = eye_height   range = { 0.45 0.55 }      }
        27 = { name = eye_height   range = { 0.55 0.6 }      }  #higher?

    }

    gene_eye_shut = {

        7 = { name = eye_shut   range = { 0.2 0.3 }      }
        27 = { name = eye_shut   range = { 0.3 0.45 }      }    #bigger eye
        64 = { name = eye_shut   range = { 0.45 0.55 }      }


    }

    gene_forehead_angle = {


        27 = { name = forehead_angle   range = { 0.3 0.45 }      }  #higher this?
        64 = { name = forehead_angle   range = { 0.45 0.55 }      }
        17 = { name = forehead_angle   range = { 0.55 0.7 }      }


    }

    gene_forehead_brow_height = {


        64 = { name = forehead_brow_height   range = { 0.5 0.6 }      }
        37 = { name = forehead_brow_height   range = { 0.6 0.8 }      }    #higher


    }

    gene_forehead_height = {

        1 = { name = forehead_height   range = { 0.1 0.2 }      }
        17 = { name = forehead_height   range = { 0.2 0.45 }      }
        64 = { name = forehead_height   range = { 0.45 0.55 }      }
        17 = { name = forehead_height   range = { 0.55 0.8 }      }
        1 = { name = forehead_height   range = { 0.8 0.9 }      }

    }

    gene_forehead_roundness = {

        1 = { name = forehead_roundness   range = { 0.1 0.2 }      }
        17 = { name = forehead_roundness   range = { 0.2 0.45 }      }
        64 = { name = forehead_roundness   range = { 0.45 0.55 }      }
        17 = { name = forehead_roundness   range = { 0.55 0.8 }      }
        1 = { name = forehead_roundness   range = { 0.8 0.9 }      }

    }

    gene_forehead_width = {

        1 = { name = forehead_width   range = { 0.1 0.2 }      }
        7 = { name = forehead_width   range = { 0.2 0.45 }      }
        84 = { name = forehead_width   range = { 0.45 0.55 }      }
        7 = { name = forehead_width   range = { 0.55 0.8 }      }
        1 = { name = forehead_width   range = { 0.8 0.9 }      }

    }

    gene_head_height = {

        47 = { name = head_height   range = { 0.35 0.45 }      }    #shorter head
        7 = { name = head_height   range = { 0.45 0.55 }      }

    }

    gene_head_profile = {


        64 = { name = head_profile   range = { 0.6 0.7 }      }
        17 = { name = head_profile   range = { 0.7 0.8 }      }    #long profiles
        1 = { name = head_profile   range = { 0.8 0.9 }      }

    }

    gene_head_top_height = {


        84 = { name = head_top_height   range = { 0.45 0.55 }      }
        37 = { name = head_top_height   range = { 0.55 0.8 }      } #big brain
        1 = { name = head_top_height   range = { 0.8 0.9 }      }

    }

    gene_head_top_width = {

        1 = { name = head_top_width   range = { 0.1 0.2 }      }
        44 = { name = head_top_width   range = { 0.2 0.4 }      }
        44 = { name = head_top_width   range = { 0.4 0.5 }      } #thinner to make <> face shape

    }

    gene_head_width = {

        1 = { name = head_width   range = { 0.1 0.2 }      }
        17 = { name = head_width   range = { 0.2 0.45 }      }
        64 = { name = head_width   range = { 0.45 0.55 }      }
        17 = { name = head_width   range = { 0.55 0.8 }      }
        1 = { name = head_width   range = { 0.8 0.9 }      }

    }

    gene_jaw_angle = {

        44 = { name = jaw_angle   range = { 0.6 0.7 }      }  #not square
        47 = { name = jaw_angle   range = { 0.7 0.8 }      }
        1 = { name = jaw_angle   range = { 0.8 0.9 }      }

    }

    gene_jaw_forward = {


        7 = { name = jaw_forward   range = { 0.3 0.45 }      }
        84 = { name = jaw_forward   range = { 0.45 0.5 }      } #never forward


    }

    gene_jaw_height = {


        84 = { name = jaw_height   range = { 0.45 0.55 }      }
        7 = { name = jaw_height   range = { 0.55 0.6 }      }


    }

    gene_jaw_width = {

        1 = { name = jaw_width   range = { 0.1 0.2 }      }
        57 = { name = jaw_width   range = { 0.2 0.3 }      }   #thin
        54 = { name = jaw_width   range = { 0.3 0.4 }      }

    }

    gene_mouth_corner_height = {


        74 = { name = mouth_corner_height   range = { 0.45 0.55 }      }
        32 = { name = mouth_corner_height   range = { 0.55 0.8 }      } #happy


    }

    gene_mouth_forward = {


        84 = { name = mouth_forward   range = { 0.45 0.55 }      }
        17 = { name = mouth_forward   range = { 0.55 0.7 }      }   #more
  

    }

    gene_mouth_height = {

        64 = { name = mouth_height   range = { 0.45 0.55 }      }   #slight chance to be higher
        17 = { name = mouth_height   range = { 0.55 0.7 }      }


    }

    gene_mouth_lower_lip_size = {

        27 = { name = mouth_lower_lip_size   range = { 0.2 0.4 }      }
        84 = { name = mouth_lower_lip_size   range = { 0.4 0.5 }      }   #thinner lips

    }

    gene_mouth_open = {

        7 = { name = mouth_open   range = { 0.2 0.45 }      }
        84 = { name = mouth_open   range = { 0.45 0.55 }      }
        7 = { name = mouth_open   range = { 0.55 0.8 }      }
        1 = { name = mouth_open   range = { 0.8 0.9 }      }

    }

    gene_mouth_upper_lip_size = {

 
        27 = { name = mouth_upper_lip_size   range = { 0.2 0.4 }      }  #thinner lips
        84 = { name = mouth_upper_lip_size   range = { 0.4 0.5 }      }

    }

    gene_mouth_width = {

        17 = { name = mouth_width   range = { 0.2 0.45 }      }
        64 = { name = mouth_width   range = { 0.6 0.7 }      }
        17 = { name = mouth_width   range = { 0.7 0.8 }      } #wider in general

    }

    gene_neck_length = {


        17 = { name = neck_length   range = { 0.3 0.45 }      }
        64 = { name = neck_length   range = { 0.45 0.55 }      }
        17 = { name = neck_length   range = { 0.55 0.6 }      }


    }

    gene_neck_width = {


        64 = { name = neck_width   range = { 0.5 0.6 }      }
        17 = { name = neck_width   range = { 0.6 0.7 }      }  #make em long boys


    }

    ### Blend shape genes ###
    gene_cheek_fat = {


        64 = { name = cheek_fat   range = { 0.45 0.55 }      }
        47 = { name = cheek_fat   range = { 0.55 0.7 }      }   #higher for laugh lines


    }

    gene_cheek_forward = {


        84 = { name = cheek_forward   range = { 0.5 0.6 }      }
        47 = { name = cheek_forward   range = { 0.6 0.9 }      }    #forward more


    }

    gene_cheek_height = {


        7 = { name = cheek_height   range = { 0.3 0.45 }      }
        84 = { name = cheek_height   range = { 0.45 0.55 }      }
        7 = { name = cheek_height   range = { 0.55 0.7 }      }


    }

    gene_cheek_prom = {


        84 = { name = cheek_prom   range = { 0.45 0.55 }      } #pretty much middle of the line, they have softer face
        7 = { name = cheek_prom   range = { 0.55 0.7 }      }


    }

    gene_cheek_width = {


        7 = { name = cheek_width   range = { 0.5 0.6 }      }
        84 = { name = cheek_width   range = { 0.6 0.7 }      }
        47 = { name = cheek_width   range = { 0.7 0.85 }      }  #WIDELADS


    }

    gene_ear_angle = {

        64 = { name = ear_angle   range = { 0.7 0.8 }      }  #lower angles
        17 = { name = ear_angle   range = { 0.8 0.9 }      }

    }

    gene_ear_inner_shape = {

        1 = { name = ear_inner_shape   range = { 0.1 0.2 }      }
        17 = { name = ear_inner_shape   range = { 0.2 0.45 }      }
        64 = { name = ear_inner_shape   range = { 0.45 0.55 }      }
        17 = { name = ear_inner_shape   range = { 0.55 0.8 }      }
        1 = { name = ear_inner_shape   range = { 0.8 0.9 }      }

    }

    gene_ear_lower_bend = {

        64 = { name = ear_lower_bend   range = { 0.6 0.7 }      }
        17 = { name = ear_lower_bend   range = { 0.7 0.8 }      }  #out
        1 = { name = ear_lower_bend   range = { 0.8 0.9 }      }

    }

    gene_ear_out = {

        64 = { name = ear_out   range = { 0.7 0.8 }      }
        37 = { name = ear_out   range = { 0.8 0.9 }      } #OUT 
        1 = { name = ear_out   range = { 0.9 1.0 }      }

    }

    gene_ear_size = {

        84 = { name = ear_size   range = { 0.7 0.8 }      }
        27 = { name = ear_size   range = { 0.8 0.9 }      } #massive
        11 = { name = ear_size   range = { 0.9 1.0 }      }

    }

    gene_ear_upper_bend = {

        44 = { name = ear_upper_bend   range = { 0.7 0.8 }      } #more chance to go out
        47 = { name = ear_upper_bend   range = { 0.8 0.9 }      }
        21 = { name = ear_upper_bend   range = { 0.9 1.0 }      }

    }

    gene_eye_corner_def = {

        1 = { name = eye_corner_def   range = { 0.1 0.2 }      }
        7 = { name = eye_corner_def   range = { 0.2 0.45 }      }
        84 = { name = eye_corner_def   range = { 0.45 0.55 }      }
        7 = { name = eye_corner_def   range = { 0.55 0.8 }      }
        1 = { name = eye_corner_def   range = { 0.8 0.9 }      }

    }

    gene_eye_corner_depth_min = {

        1 = { name = eye_corner_depth_min   range = { 0.1 0.2 }      }
        17 = { name = eye_corner_depth_min   range = { 0.2 0.45 }      }
        64 = { name = eye_corner_depth_min   range = { 0.45 0.55 }      }
        17 = { name = eye_corner_depth_min   range = { 0.55 0.8 }      }
        1 = { name = eye_corner_depth_min   range = { 0.8 0.9 }      }

    }

    gene_eye_fold_droop = {

        1 = { name = eye_fold_droop   range = { 0.1 0.2 }      }
        17 = { name = eye_fold_droop   range = { 0.2 0.45 }      }
        64 = { name = eye_fold_droop   range = { 0.45 0.55 }      }
        17 = { name = eye_fold_droop   range = { 0.55 0.8 }      }
        1 = { name = eye_fold_droop   range = { 0.8 0.9 }      }

    }

    gene_eye_fold_shape = {

        1 = { name = eye_fold_shape   range = { 0.1 0.2 }      }
        17 = { name = eye_fold_shape   range = { 0.2 0.45 }      }
        64 = { name = eye_fold_shape   range = { 0.45 0.55 }      }
        17 = { name = eye_fold_shape   range = { 0.55 0.8 }      }
        1 = { name = eye_fold_shape   range = { 0.8 0.9 }      }

    }

    gene_eye_size = {


        17 = { name = eye_size   range = { 0.3 0.4 }      }
        64 = { name = eye_size   range = { 0.4 0.45 }      }   #smaller in general
        17 = { name = eye_size   range = { 0.45 0.55 }      }


    }

    gene_eye_upper_lid_size = {

        1 = { name = eye_upper_lid_size   range = { 0.1 0.2 }      }
        17 = { name = eye_upper_lid_size   range = { 0.2 0.45 }      }
        64 = { name = eye_upper_lid_size   range = { 0.45 0.55 }      }
        17 = { name = eye_upper_lid_size   range = { 0.55 0.8 }      }
        1 = { name = eye_upper_lid_size   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_curve = {

        64 = { name = forehead_brow_curve   range = { 0.45 0.55 }      }
        37 = { name = forehead_brow_curve   range = { 0.55 0.8 }      } #arched
        1 = { name = forehead_brow_curve   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_forward = {

        1 = { name = forehead_brow_forward   range = { 0.1 0.2 }      }
        7 = { name = forehead_brow_forward   range = { 0.2 0.45 }      }
        84 = { name = forehead_brow_forward   range = { 0.45 0.55 }      }
        7 = { name = forehead_brow_forward   range = { 0.55 0.8 }      }
        1 = { name = forehead_brow_forward   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_inner_height = {

        1 = { name = forehead_brow_inner_height   range = { 0.1 0.2 }      }
        47 = { name = forehead_brow_inner_height   range = { 0.2 0.45 }      }
        44 = { name = forehead_brow_inner_height   range = { 0.45 0.55 }      }
        7 = { name = forehead_brow_inner_height   range = { 0.55 0.8 }      }
    }

    gene_forehead_brow_outer_height = {

        84 = { name = forehead_brow_outer_height   range = { 0.6 0.7 }      } #high
        47 = { name = forehead_brow_outer_height   range = { 0.7 0.8 }      }
        21 = { name = forehead_brow_outer_height   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_width = {

        1 = { name = forehead_brow_width   range = { 0.1 0.2 }      }
        47 = { name = forehead_brow_width   range = { 0.2 0.45 }      }
        84 = { name = forehead_brow_width   range = { 0.45 0.55 }      }    #lower width for <> shape

    }

    gene_forehead_roundness = {

        1 = { name = forehead_roundness   range = { 0.1 0.2 }      }
        17 = { name = forehead_roundness   range = { 0.2 0.45 }      }  #lower roundness
        64 = { name = forehead_roundness   range = { 0.45 0.55 }      }

    }

    gene_jaw_def = {

        64 = { name = jaw_def   range = { 0.45 0.55 }      }
        27 = { name = jaw_def   range = { 0.55 0.8 }      } #more chance of def
        1 = { name = jaw_def   range = { 0.8 0.9 }      }

    }

    gene_mouth_lower_lip_def = {

        1 = { name = mouth_lower_lip_def   range = { 0.1 0.2 }      }
        27 = { name = mouth_lower_lip_def   range = { 0.2 0.45 }      } #higher chance for not
        64 = { name = mouth_lower_lip_def   range = { 0.45 0.55 }      }
        17 = { name = mouth_lower_lip_def   range = { 0.55 0.8 }      }
        1 = { name = mouth_lower_lip_def   range = { 0.8 0.9 }      }

    }

    gene_mouth_lower_lip_full = {

        1 = { name = mouth_lower_lip_full   range = { 0.1 0.2 }      }
        17 = { name = mouth_lower_lip_full   range = { 0.2 0.45 }      }    #higher chance for not
        64 = { name = mouth_lower_lip_full   range = { 0.45 0.55 }      }

    }

    gene_mouth_lower_lip_pads = {

        1 = { name = mouth_lower_lip_pads   range = { 0.1 0.2 }      }
        17 = { name = mouth_lower_lip_pads   range = { 0.2 0.45 }      }    #higher chance for not
        64 = { name = mouth_lower_lip_pads   range = { 0.45 0.55 }      }

    }

    gene_mouth_lower_lip_width = {

        1 = { name = mouth_lower_lip_width   range = { 0.1 0.2 }      }
        27 = { name = mouth_lower_lip_width   range = { 0.2 0.3 }      }    #lower width
        84 = { name = mouth_lower_lip_width   range = { 0.3 0.5 }      }

    }

    gene_mouth_philtrum_curve = {

        1 = { name = mouth_philtrum_curve   range = { 0.1 0.2 }      }
        17 = { name = mouth_philtrum_curve   range = { 0.2 0.45 }      }
        64 = { name = mouth_philtrum_curve   range = { 0.45 0.55 }      }
        17 = { name = mouth_philtrum_curve   range = { 0.55 0.8 }      }
        1 = { name = mouth_philtrum_curve   range = { 0.8 0.9 }      }

    }

    gene_mouth_philtrum_def = {

        1 = { name = mouth_philtrum_def   range = { 0.1 0.2 }      }
        17 = { name = mouth_philtrum_def   range = { 0.2 0.45 }      }
        64 = { name = mouth_philtrum_def   range = { 0.45 0.55 }      }
        27 = { name = mouth_philtrum_def   range = { 0.55 0.8 }      }  #slightly higher chance
        1 = { name = mouth_philtrum_def   range = { 0.8 0.9 }      }

    }

    gene_mouth_philtrum_width = {

        1 = { name = mouth_philtrum_width   range = { 0.1 0.2 }      }
        7 = { name = mouth_philtrum_width   range = { 0.2 0.45 }      } #thin
        84 = { name = mouth_philtrum_width   range = { 0.45 0.55 }      }

    }

    gene_mouth_upper_lip_curve = {

        1 = { name = mouth_upper_lip_curve   range = { 0.1 0.2 }      } #low
        17 = { name = mouth_upper_lip_curve   range = { 0.2 0.3 }      }
        64 = { name = mouth_upper_lip_curve   range = { 0.3 0.4 }      }

    }

    gene_mouth_upper_lip_def = {

        1 = { name = mouth_upper_lip_def   range = { 0.1 0.2 }      }
        17 = { name = mouth_upper_lip_def   range = { 0.2 0.45 }      }
        64 = { name = mouth_upper_lip_def   range = { 0.45 0.55 }      }
        17 = { name = mouth_upper_lip_def   range = { 0.55 0.8 }      }
        1 = { name = mouth_upper_lip_def   range = { 0.8 0.9 }      }

    }

    gene_mouth_upper_lip_full = {

        1 = { name = mouth_upper_lip_full   range = { 0.1 0.2 }      }
        17 = { name = mouth_upper_lip_full   range = { 0.2 0.45 }      }    #thinner in general
        64 = { name = mouth_upper_lip_full   range = { 0.45 0.55 }      }

    }

    gene_mouth_upper_lip_width = {

        84 = { name = mouth_upper_lip_width   range = { 0.45 0.55 }      }
        7 = { name = mouth_upper_lip_width   range = { 0.55 0.8 }      }    #contrast to lower, these are longer width chance
        1 = { name = mouth_upper_lip_width   range = { 0.8 0.9 }      }

    }

    gene_nose_curve = {

        1 = { name = nose_curve   range = { 0.1 0.2 }      }
        55 = { name = nose_curve   range = { 0.2 0.3 }      }
        55 = { name = nose_curve   range = { 0.3 0.4 }      } #high curve

    }

    gene_nose_forward = {

        12 = { name = nose_forward   range = { 0.2 0.45 }      }
        74 = { name = nose_forward   range = { 0.45 0.55 }      }   #standard
        12 = { name = nose_forward   range = { 0.55 0.7 }      }

    }

    gene_nose_hawk = {

        1 = { name = nose_hawk   range = { 0.1 0.2 }      }
        55 = { name = nose_hawk   range = { 0.2 0.45 }      }   #no

    }

    gene_nose_height = {


        17 = { name = nose_height   range = { 0.3 0.45 }      }
        64 = { name = nose_height   range = { 0.45 0.55 }      }    #never too high


    }

    gene_nose_length = {

        17 = { name = nose_length   range = { 0.7 0.8 }      }
        64 = { name = nose_length   range = { 0.8 0.9 }      }    #big
        27 = { name = nose_length   range = { 0.9 1.0 }      }


    }

    gene_nose_nostril_angle = {

        1 = { name = nose_nostril_angle   range = { 0.1 0.2 }      }
        47 = { name = nose_nostril_angle   range = { 0.2 0.3 }      }
        44 = { name = nose_nostril_angle   range = { 0.3 0.4 }      } #low angle

    }

    gene_nose_nostril_height = {

        1 = { name = nose_nostril_height   range = { 0.1 0.2 }      }
        7 = { name = nose_nostril_height   range = { 0.2 0.45 }      }
        84 = { name = nose_nostril_height   range = { 0.45 0.55 }      }
        7 = { name = nose_nostril_height   range = { 0.55 0.8 }      }
        1 = { name = nose_nostril_height   range = { 0.8 0.9 }      }

    }

    gene_nose_nostril_width = {


        7 = { name = nose_nostril_width   range = { 0.4 0.7 }      }    #small chance of more button nose
        44 = { name = nose_nostril_width   range = { 0.7 0.8 }      }
        47 = { name = nose_nostril_width   range = { 0.8 1.0}      }    #schlongers otherwise


    }

    gene_nose_ridge_angle = {

        24 = { name = nose_ridge_angle   range = { 0.7 0.8 }      }
        64 = { name = nose_ridge_angle   range = { 0.8 0.9 }      }
        21 = { name = nose_ridge_angle   range = { 0.9 1.0 }      }  #high

    }

    gene_nose_ridge_def = {

        1 = { name = nose_ridge_def   range = { 0.1 0.2 }      }
        7 = { name = nose_ridge_def   range = { 0.2 0.45 }      }   #not strong
        84 = { name = nose_ridge_def   range = { 0.45 0.55 }      }

    }

    gene_nose_ridge_def_min = {

        84 = { name = nose_ridge_def_min   range = { 0.45 0.55 }      }
        17 = { name = nose_ridge_def_min   range = { 0.55 0.8 }      }   #can allow for this
        1 = { name = nose_ridge_def_min   range = { 0.8 0.9 }      }

    }

    gene_nose_ridge_width = {

        0 = { name = nose_ridge_width   range = { 0.1 0.2 }      }
        47 = { name = nose_ridge_width   range = { 0.2 0.45 }      } #schlongers but thin schlongers
        84 = { name = nose_ridge_width   range = { 0.45 0.55 }      }

    }

    gene_nose_size = {

        25 = { name = nose_size   range = { 0.7 0.8 }      }  #schlongkings
        50 = { name = nose_size   range = { 0.8 0.9 }      }
        25 = { name = nose_size   range = { 0.9 1.0 }      }

    }

    gene_nose_tip_angle = {

        31 = { name = nose_tip_angle   range = { 0.04 0.2 }      }
        37 = { name = nose_tip_angle   range = { 0.2 0.45 }      }   #low
        55 = { name = nose_tip_angle   range = { 0.45 0.55 }      }

    }

    gene_nose_tip_forward = {

        84 = { name = nose_tip_forward   range = { 0.45 0.55 }      }
        37 = { name = nose_tip_forward   range = { 0.55 0.8 }      }    #its gonna be more forward than not
        1 = { name = nose_tip_forward   range = { 0.8 0.9 }      }

    }

    gene_nose_tip_width = {

        17 = { name = nose_tip_width   range = { 0.7 0.8 }      }
        84 = { name = nose_tip_width   range = { 0.8 0.9 }      }   #foreverbulbous
        17 = { name = nose_tip_width   range = { 0.9 1.0 }      }

    }

	gene_bs_body_type = {

        7 = { name = body_fat_head_fat_low   range = { 0.2 0.45 }      }
        20 = { name = body_fat_head_fat_low   range = { 0.45 0.55 }     }
        7 = { name = body_fat_head_fat_low   range = { 0.55 0.8 }      }

        7 = { name = body_fat_head_fat_medium   range = { 0.2 0.45 }      }
        20 = { name = body_fat_head_fat_medium   range = { 0.45 0.55 }      }
        7 = { name = body_fat_head_fat_medium   range = { 0.55 0.8 }      }

        7 = { name = body_fat_head_fat_full   range = { 0.2 0.45 }      }
        20 = { name = body_fat_head_fat_full   range = {0.45 0.55 }      }
        7 = { name = body_fat_head_fat_full   range = { 0.55 0.8 }      }

	}

	gene_height = {

        20 = { name = short_height  range = { 0.1 0.2  }      }
        50 = { name = short_height  range = { 0.2 0.3 }      }
        20 = { name = short_height  range = { 0.3 0.5 }      }

	}

	gene_old_eyes = {

		10 = { name = old_eyes_01   range = { 1.0 1.0 }      }
		10 = { name = old_eyes_02   range = { 1.0 1.0 }      }
		10 = { name = old_eyes_03   range = { 1.0 1.0 }      }
	}

	gene_old_forehead = {

		10 = { name = old_forehead_01   range = { 1.0 1.0 }      }
		10 = { name = old_forehead_02   range = { 1.0 1.0 }      }
		10 = { name = old_forehead_03   range = { 1.0 1.0 }      }
	}

	gene_old_mouth = {

		10 = { name = old_mouth_01   range = { 1.0 1.0 }      }
		10 = { name = old_mouth_02   range = { 1.0 1.0 }      }
		10 = { name = old_mouth_03   range = { 1.0 1.0 }      }
	}

	gene_complexion = {

		10 = { name = complexion_01   range = { 0.0 0.0 }      }
		10 = { name = complexion_02   range = { 0.0 0.0 }      }
		10 = { name = complexion_03   range = { 0.0 0.0 }      }
	}

	gene_crowfeet = {

		9 = { name = crowfeet_01   range = { 0.0 0.1 }      }
		1 = { name = crowfeet_01   range = { 0.1 0.8 }      }
		9 = { name = crowfeet_02   range = { 0.0 0.1 }      }
		1 = { name = crowfeet_02   range = { 0.1 0.8 }      }
		9 = { name = crowfeet_03   range = { 0.0 0.1 }      }
		1 = { name = crowfeet_03   range = { 0.1 0.8 }      }
	}

	gene_frown = {

		8 = { name = frown_01   range = { 0.0 0.1 }      }
		2 = { name = frown_01   range = { 0.1 0.8 }      }
		8 = { name = frown_02   range = { 0.0 0.1 }      }
		2 = { name = frown_02   range = { 0.1 0.8 }      }
		8 = { name = frown_03   range = { 0.0 0.1 }      }
		2 = { name = frown_03   range = { 0.1 0.8 }      }
	}

	gene_surprise = {

		9 = { name = surprise_01   range = { 0.0 0.1 }      }
		1 = { name = surprise_01   range = { 0.1 0.8 }      }
		9 = { name = surprise_02   range = { 0.0 0.1 }      }
		1 = { name = surprise_02   range = { 0.1 0.8 }      }
		9 = { name = surprise_03   range = { 0.0 0.1 }      }
		1 = { name = surprise_03   range = { 0.1 0.8 }      }
	}

    gene_eyebrows_shape = {
        10 = {  name = avg_spacing_avg_thickness             range = { 0.5 1.0 }     }
        2 = {  name = avg_spacing_high_thickness             range = { 0.5 1.0 }     }
        10 = {  name = avg_spacing_low_thickness             range = { 0.5 1.0 }    }
        10 = {  name = avg_spacing_lower_thickness             range = { 0.5 1.0 }    }

        10 = {  name = far_spacing_avg_thickness             range = { 0.5 1.0 }     }
        2 = {  name = far_spacing_high_thickness             range = { 0.5 1.0 }     }
        10 = {  name = far_spacing_low_thickness             range = { 0.5 1.0 }    }
        10 = {  name = far_spacing_lower_thickness             range = { 0.5 1.0 }    } #removed neanderthal ones

        10 = {  name = close_spacing_avg_thickness             range = { 0.5 1.0 }     }
        10 = {  name = close_spacing_low_thickness             range = { 0.5 1.0 }    }
        10 = {  name = close_spacing_lower_thickness             range = { 0.5 1.0 }    }
    }

    gene_eyebrows_fullness = {
        10 = {  name = layer_2_avg_thickness             range = { 0.25 0.5 }     }
        15 = {  name = layer_2_avg_thickness             range = { 0.5 0.75 }     }
        5 = {  name = layer_2_high_thickness             range = { 0.25 0.5 }     }
        5 = {  name = layer_2_high_thickness             range = { 0.5 0.75 }     }
        10 = {  name = layer_2_low_thickness             range = { 0.25 0.5 }     }
        15 = {  name = layer_2_low_thickness             range = { 0.5 0.75 }     }
        10 = {  name = layer_2_lower_thickness             range = { 0.25 0.5 }     }
        15 = {  name = layer_2_lower_thickness             range = { 0.5 0.75 }     }
    }

    gene_face_dacals = {

        10 = { name = face_dacal_01   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_02   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_03   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_04   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_05   range = { 1.0 1.0 }      }
 
    }

	hairstyles = {
		10 = { name = european_hairstyles 		range = { 0.0 1.0 } }
	}

	beards = {
		10 = { name = european_beards 		    range = { 0.0 1.0 } }
        15 = { name = no_beard                      range = { 0.0 1.0 } }
	}

	mustaches = {
		10 = { name = european_mustaches 		range = { 0.0 1.0 } }
        10 = { name = no_mustache       range = { 0.0 1.0 } }
	}

	# coats = {
	# 	10 = { name = prussian_coats		range = { 0.0 1.0 } }
	# }

	# epaulettes = {
	# 	10 = { name = prussian_epaulettes		range = { 0.0 1.0 } }
	# }

	# sashes = {
	# 	10 = { name = prussian_sashes		range = { 0.0 1.0 } }
	# }

	# medals = {
	# 	10 = { name = all_medals		range = { 0.0 1.0 } }
	# }

	# headgear = {
	# 	10 = { name = generic_headgear		range = { 0.0 1.0 } }
	# }


    #Anbennar Additions my friends

	POD_NOSschnoz = {
		10 = { name = nosies 		range = { 0.0 0.0 } }
        10 = { name = schnozies       range = { 0.0 0.0 } }
	}

	race_gene_mer_ears_01 = {
        75 = {  name = mer_ears_03             range = { 0.1 0.5 }     }
    }
    race_gene_mer_ears_02 = {
        75 = {  name = mer_ears_03             range = { 0.1 0.5 }     }
    }


}
