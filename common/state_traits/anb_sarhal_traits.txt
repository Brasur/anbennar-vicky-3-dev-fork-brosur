﻿#Deserts
state_trait_sandspite_desert = {
	icon = "gfx/interface/icons/state_trait_icons/dry_climate.dds"

	modifier = {
        building_group_bg_agriculture_throughput_add = -0.2
		state_construction_mult = -0.25
		state_infrastructure_mult = -0.25
    }
}

state_trait_ardimya_desert = {
	icon = "gfx/interface/icons/state_trait_icons/dry_climate.dds"

	modifier = {
        building_group_bg_agriculture_throughput_add = -0.2
		state_construction_mult = -0.25
		state_infrastructure_mult = -0.25
    }
}

#Rivers
state_trait_mothers_sorrow = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"

	modifier = {
		building_group_bg_agriculture_throughput_add = 0.15
		building_group_bg_plantations_throughput_add = 0.15
		state_infrastructure_add = 20
		state_market_access_price_impact = 0.05
	}
}