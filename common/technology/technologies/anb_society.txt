﻿### ERA 1

### ERA 2

### ERA 3

locked = {
	# exists to stop buildings etc
	era = era_1
	#texture = "gfx/interface/icons/invention_icons/urbanization.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

psionic_theory = {
	# Unlocks Urban Centers building
	era = era_3
	#texture = "gfx/interface/icons/invention_icons/urbanization.dds"
	category = society

	unlocking_technologies = {
		psychiatry
		dialectics
		pharmaceuticals
	}
	
	ai_weight = {
		value = 1
	}
}

planar_portals = {
	# Unlocks Urban Centers building
	era = era_3
	#texture = "gfx/interface/icons/invention_icons/urbanization.dds"
	category = society

	unlocking_technologies = {
		psionic_theory
		philosophical_pragmatism
	}
	
	ai_weight = {
		value = 1
	}
}

interplanetary_portals = {
	# Unlocks Urban Centers building
	era = era_3
	#texture = "gfx/interface/icons/invention_icons/urbanization.dds"
	category = society

	unlocking_technologies = {
		psionic_theory
		civilizing_mission
	}
	
	ai_weight = {
		value = 1
	}
}


### ERA 4

automated_translators = {
	# Unlocks Urban Centers building
	era = era_4
	#texture = "gfx/interface/icons/invention_icons/urbanization.dds"
	category = society

	unlocking_technologies = {
		identification_documents
		pan-nationalism	#rare - moment
		civilizing_mission
	}
	
	ai_weight = {
		value = 1
	}
}


### ERA 5
department_stores = {
	# Unlocks Urban Centers building
	era = era_5
	texture = "gfx/interface/icons/invention_icons/urbanization.dds"
	category = society

	unlocking_technologies = {
		elevator	#as it provides direct upgrades
	}
	
	ai_weight = {
		value = 1
	}
}

planar_philosophy = {
	# Unlocks Urban Centers building
	era = era_5
	#texture = "gfx/interface/icons/invention_icons/urbanization.dds"
	category = society

	unlocking_technologies = {
		analytical_philosophy	#as it provides direct upgrades
	}
	
	ai_weight = {
		value = 1
	}
}

