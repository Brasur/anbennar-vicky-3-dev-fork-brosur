﻿BUILDINGS={
	s:STATE_IBTAT={
		region_state:F02={
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_wheat_farm"
						country="c:F02"
						levels=2
						region="STATE_IBTAT"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:F02"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

}