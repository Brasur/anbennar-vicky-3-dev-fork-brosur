﻿race_is_accepted_by_racial_laws_tt = {	
	global = TRIGGER_RACE_IS_ACCEPTED_BY_RACIAL_LAWS
	global_not = TRIGGER_RACE_IS_ACCEPTED_BY_RACIAL_LAWS_NOT
}

is_same_race_as_any_primary_culture_tt = {
	global = TRIGGER_IS_SAME_RACE_AS_PRIMARY_CULTURE
	global_not = TRIGGER_IS_SAME_RACE_AS_PRIMARY_CULTURE_NOT
}

is_same_race_as_any_primary_culture_or_human_tt = {
	global = TRIGGER_IS_SAME_RACE_AS_PRIMARY_CULTURE_OR_HUMAN
	global_not = TRIGGER_IS_SAME_RACE_AS_PRIMARY_CULTURE_OR_HUMAN_NOT
}

is_monstrous_race_tt = {
	global = TRIGGER_IS_MONSTROUS_RACE
	global_not = TRIGGER_IS_NON_MONSTROUS_RACE
}

is_non_monstrous_race_tt = {
	global = TRIGGER_IS_NON_MONSTROUS_RACE
	global_not = TRIGGER_IS_MONSTROUS_RACE
}

is_goblinoid_group_tt = {
	global = TRIGGER_IS_GOBLINOID_GROUP
	global_not = TRIGGER_IS_GOBLINOID_GROUP_NOT
}

is_goblinoid_group_or_human_tt = {
	global = TRIGGER_IS_GOBLINOID_GROUP_OR_HUMAN
	global_not = TRIGGER_IS_GOBLINOID_GROUP_OR_HUMAN_NOT
}

is_giantkin_group_tt = {
	global = TRIGGER_IS_GIANTKIN_GROUP
	global_not = TRIGGER_IS_GIANTKIN_GROUP_NOT
}

is_giantkin_group_or_human_tt = {
	global = TRIGGER_IS_GIANTKIN_GROUP_OR_HUMAN
	global_not = TRIGGER_IS_GIANTKIN_GROUP_OR_HUMAN_NOT
}

is_ruinborn_group_tt = {
	global = TRIGGER_IS_RUINBORN_GROUP
	global_not = TRIGGER_IS_RUINBORN_GROUP_NOT
}

is_ruinborn_group_or_human_tt = {
	global = TRIGGER_IS_RUINBORN_GROUP_OR_HUMAN
	global_not = TRIGGER_IS_RUINBORN_GROUP_OR_HUMAN_NOT
}

is_human_race_tt = {
	global = TRIGGER_IS_HUMAN_RACE
	global_not = TRIGGER_IS_HUMAN_RACE_NOT
}