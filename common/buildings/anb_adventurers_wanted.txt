﻿#building_adventurers_wanted_dungeon_brigand_army_base = {
#	building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#	city_type = none
#	levels_per_mesh = 0
#	buildable = no
#	expandable = no
#	downsizeable = no
#
#	production_method_groups = {
#		pmg_basic_equipment_building_brigand_army_base
#		pmg_civilian_infrastructure_building_brigand_army_base
#		pmg_delving_support_building_brigand_army_base
#		pmg_state_involvement_bg_adventurers_wanted
#	}
#}
#
#building_adventurers_wanted_dungeon_lich_lair = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_lich_lair
#        pmg_civilian_infrastructure_building_lich_lair
#        pmg_delving_support_building_lich_lair
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_dungeon_precursor_base = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_precursor_base
#        pmg_civilian_infrastructure_building_precursor_base
#        pmg_delving_support_building_precursor_base
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_dungeon_sea_monster_nest = {
#    building_group = bg_adventurers_wanted
#    texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_sea_monster_nest
#        pmg_civilian_infrastructure_building_sea_monster_nest
#        pmg_naval_equipment_building_sea_monster_nest
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_sudden_threat_den_of_beasts = {
#    building_group = bg_adventurers_wanted
#    texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_den_of_beasts
#        pmg_extra_firepower_building_den_of_beasts
#        pmg_auxiliary_support_building_den_of_beasts
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_sudden_threat_undead_mob = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_undead_mob
#        pmg_extra_firepower_building_undead_mob
#        pmg_auxiliary_support_building_undead_mob
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_sudden_threat_planar_breach = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_planar_breach
#        pmg_extra_firepower_building_planar_breach
#        pmg_auxiliary_support_building_planar_breach
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_sudden_threat_kraken_attack = {
#    building_group = bg_adventurers_wanted
#    texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_kraken_attack
#        pmg_naval_equipment_building_kraken_attack
#        pmg_auxiliary_support_building_kraken_attack
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_infestation_fey_incursion = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_base.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_fey_incursion
#        pmg_extermination_methods_building_fey_incursion
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_infestation_awakened_treants = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_base.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_awakened_treants
#        pmg_extermination_methods_building_awakened_treants
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_infestation_ancient_golems = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_base.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_ancient_golems
#        pmg_extermination_methods_building_ancient_golems
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_infestation_elemental_forge = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/adventurers_wanted_base.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_basic_equipment_building_elemental_forge
#        pmg_extermination_methods_building_elemental_forge
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_investigation_thieves_guild = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/urban_center.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_investigation_methods_building_thieves_guild
#        pmg_appeasement_level_bg_adventurers_wanted
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_investigation_hag_dens = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/urban_center.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_investigation_methods_building_hag_dens
#        pmg_appeasement_level_bg_adventurers_wanted
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_investigation_eldritch_cult = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/urban_center.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_investigation_methods_building_eldritch_cult
#        pmg_appeasement_level_bg_adventurers_wanted
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}
#
#building_adventurers_wanted_investigation_vampire_family = {
#    building_group = bg_adventurers_wanted
#	texture = "gfx/interface/icons/building_icons/urban_center.dds"
#    city_type = none
#    levels_per_mesh = 0
#    buildable = no
#    expandable = no
#    downsizeable = no
#
#    production_method_groups = {
#        pmg_investigation_methods_building_vampire_family
#        pmg_appeasement_level_bg_adventurers_wanted
#        pmg_state_involvement_bg_adventurers_wanted
#    }
#}

# building_adventurers_wanted_base = {
# 	building_group = bg_adventurers_wanted
	# texture = "gfx/interface/icons/building_icons/adventurers_wanted_base.dds"
# 	city_type = none
# 	levels_per_mesh = 5
# 	buildable = no
# 	expandable = no
# 	downsizeable = no

# 	production_method_groups = {
# 		pmg_base_building_adventurers_wanted
# 		pmg_party_composition_building_adventurers_wanted
# 		pmg_loot_share_building_adventurers_wanted
# 	}
# }


# building_adventurers_wanted_dungeon_ancient_catacombs = {
# 	building_group = bg_adventurers_wanted
	# texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
# 	city_type = none
# 	levels_per_mesh = 5
# 	buildable = no
# 	expandable = no
# 	downsizeable = no

# 	production_method_groups = {
# 		pmg_dungeon_ancient_catacombs_building_adventurers_wanted

# 		pmg_dungeon_party_composition_building_adventurers_wanted

# 		pmg_loot_share_building_adventurers_wanted
# 	}
# }

# building_adventurers_wanted_combat_dragon = {
# 	building_group = bg_adventurers_wanted
# 	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_dragon.dds"
# 	city_type = none
# 	levels_per_mesh = 5
# 	buildable = no
# 	expandable = no
# 	downsizeable = no

# 	production_method_groups = {
# 		pmg_combat_dragon_building_adventurers_wanted

# 		pmg_combat_party_composition_building_adventurers_wanted
# 		pmg_loot_share_building_adventurers_wanted
# 	}
# }