﻿alecand_reclamation_progress_bar = {
	name = "alecand_reclamation_progress_bar"
	desc = "alecand_reclamation_progress_bar_desc"

	default = yes

	monthly_progress = {
		if = {
			limit = {
				has_technology_researched = punch_card_artificery
			}
			add = {
				desc = "anb_reclamation_punch_card_artificery_tt"
				value = 1
			}
		}

		if = {
			limit = {
				has_technology_researched = elemental_elicitation
			}
			add = {
				desc = "anb_reclamation_elemental_elicitation_tt"
				value = 1
			}
		}

		if = {
			limit = {
				has_technology_researched = magic_wave_stabilization
			}
			add = {
				desc = "anb_reclamation_magic_wave_stabilization_tt"
				value = 1
			}
		}

		if = {
			limit = {
				has_technology_researched = thaumic_tearite
			}
			add = {
				desc = "anb_reclamation_thaumic_tearite_tt"
				value = 1
			}
		}
		if = {
			limit = {
				has_variable = var_innovation_reclamation_button_spent_var
			}
			add = {
				desc = "anb_reclamation_buttons_innovation_tt"
				value = 1
			}
		}

		if = {
			limit = {
				has_variable = var_construction_reclamation_button_spent_var
			}
			add = {
				desc = "anb_reclamation_buttons_construction_tt"
				value = 2
			}
		}

		add = {
          	desc = "anb_reclamation_goods_tt"
          	market_capital.market = {
              	mg:artificery_doodads = {
                  	if = {
                      	limit = {
                          	market_goods_production >= 500
                      	}
                      	add = 1
                  	}
                  	else = {
                      	add = market_goods_production
                      	divide = 500
                  	}
              }
          }
        
		}
	}

	start_value = 0
	min_value = 0
	max_value = 700
} 